
# Error codes

| Code | Status | Description                                    |
|-----:|:------:|------------------------------------------------|
| `-1` | `500`  | Undefined.                                     |
|  `0` | `404`  | User not found.                                |
|  `1` | `400`  | The given data was invalid.                    |
|  `2` | `404`  | The requested data was not found.              |
|  `3` | `400`  | User with the same number already exists.      |
|  `4` | `403`  | Invalid credentials.                           |
|  `5` | `403`  | Not allowed to modify other users products.    |
|  `6` | `403`  | Not allowed to delete comments of other users. |
|  `7` | `400`  | The request is missing image data.             |
