
# Mahalna

Have you got any products that will expire soon and don't need them?
Other might do! And they could also pay for it 😉

That's why Mahalna has been made.

Bruh. You didn't believe that right??? It's a low-quality project required by the "Programming Languages" course in the Faculty of Information Technology Engineering Faculty at Damascus University 🤡

## Installation for Developers

### Pre-requests

It's expected that you know how to and already have installed:

- A recent version of XAMPP with PHP 8.
- Installed a recent version of the php composer.
- Added the php included in XAMPP to your `PATH` variable.
- Started the MySQL server from the XAMPP control panel.
- Didn't change the default password of XAMPP nor the default configuration.
- Know how to use git.

### Instructions

1. Clone the repository to somewhere locally.
2. Open a terminal in the root of the repository's directory.
3. Install the PHP dependencies by typing `composer install`.
4. Copy the `.env.example` file into a newly created file `.env` at the root of the repository.
5. Open the admin panel of the MySQL database and create an empty database with the name `mahalna_backend`.
6. Run `php artisan migrate:fresh` to setup the database.
7. Start the local development server using `php artisan serve --host 0.0.0.0`

### Updating

1. Fetch the latest commits from git remote (run git pull).
2. Run `php artisan migrate:fresh` to reset and rebuild the database.

## Frequently Asked Questions

### What's the baseUrl for the local server?

- For an application running on the same device: `localhost:8000/api`.
- For other devices on the same network:
    - Figure out the IP of the device running the server (using `ipconfig` on Windows).
    - It's then `192.168.1.<something>:8000/api`.
