<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Kra8\Snowflake\HasSnowflakePrimary;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasSnowflakePrimary;

    protected $table = 'users';
    public $timestamps = false;

    protected $fillable = ['name', 'phone', 'password'];
    protected $hidden = ['password'];
}
