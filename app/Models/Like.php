<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kra8\Snowflake\HasSnowflakePrimary;

class Like extends Model
{
    use HasFactory;
    use HasSnowflakePrimary;

    protected $table = 'likes';
    public $timestamps = false;
    protected $fillable = ['product_id', 'user_id'];
}
