<?php

namespace App\Models;

use Kra8\Snowflake\HasSnowflakePrimary;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    use HasSnowflakePrimary;

    protected $table = 'products';
    public $timestamps = false;
    protected $fillable = [
        'image_id', 'created_by_id', 'name', 'description', 'category', 'contact_phone',
        'expiry_date', 'unit_price', 'available_quantity',
        'initial_sale', 'first_period_days', 'first_period_sale',
        'second_period_days', 'second_period_sale',
    ];
    protected $attributes = [
        'views_count' => 0,
        'likes_count' => 0,
    ];

    public function comments() {
        return $this->hasMany(Comment::class);
    }
}
