<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kra8\Snowflake\HasSnowflakePrimary;

class Comment extends Model
{
    use HasFactory;
    use HasSnowflakePrimary;

    protected $table = 'comments';
    public $timestamps = false;
    protected $fillable = ['user_id', 'product_id', 'content'];
}
