<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kra8\Snowflake\HasSnowflakePrimary;

class Image extends Model
{
    use HasFactory;
    use HasSnowflakePrimary;

    protected $table = 'images';
    public $timestamps = false;
    protected $fillable = ['data'];
}
