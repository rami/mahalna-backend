<?php

namespace App\Exceptions;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (Throwable $e, $request) {
            $status_code = 500; // Internal Server Error
            $error_code = -1; // Undefined Error

            if ($e instanceof ApiException) {
                $status_code = $e->status_code;
                $error_code = $e->getCode();
            } else if ($e instanceof ValidationException) {
                $status_code = 400; // Bad request
                $error_code = 1; // The given data was invalid
            } else if ($e instanceof ModelNotFoundException || str_starts_with($e->getMessage(), 'No query results for model')) {
                $status_code = 404; // Not found
                $error_code = 2; // The requested data was not found
            }

            return response()->json([
                'code' => $error_code,
                'debug' => [
                    'message' => $e->getMessage(),
                    'stacktrace' => $e->getTrace(),
                ],
            ], $status_code);
        });
    }
}
