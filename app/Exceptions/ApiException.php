<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class ApiException extends Exception
{
    public int $status_code;

    /**
     * @param int $status_code The HTTP status code of the response.
     * @param int $error_code The code of the error message to display for the user.
     * @param string $message The debug message hinting the reason of the error for the developers.
     */
    public function __construct(int $status_code = 400, int $error_code = -1, string $message = 'Undefined error', ?Throwable $previous = null)
    {
        $this->status_code = $status_code;
        parent::__construct($message, $error_code, $previous);

    }
}
