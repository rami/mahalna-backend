<?php

namespace App\Http\Controllers;

use App\Exceptions\ApiException;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        // Validate the requested parameters.
        $validated = $request->validate([
            'name' => 'required|string|filled',
            'phone' => 'required|string|regex:/(^\+963\d{9}$)/',
            'password' => 'required|string|min:8',
        ]);

        // Check if a user with the same number already exists.
        $existing_user = User::firstWhere('phone', $validated['phone']);
        if (!is_null($existing_user)) throw new ApiException(400, 3, 'User with the same phone number already exists');

        // Hash the users password in case of leaks.
        $validated['password'] = Hash::make($validated['password']);

        // Create the new user.
        $user = User::create($validated);

        return [
            "token" => $user->createToken('DEVICE_NAME')->plainTextToken,
            "user" => $user,
        ];
    }

    public function login(Request $request)
    {
        $validated = $request->validate([
            'phone' => 'required|string|regex:/(^\+963\d{9}$)/',
            'password' => 'required|string|min:8',
        ]);

        $user = User::firstWhere('phone', $validated['phone']);
        if (is_null($user) || !Hash::check($validated['password'], $user->password))
            throw new ApiException(403, 4, 'Invalid credentials');
        
        return [
            "token" => $user->createToken('DEVICE_NAME')->plainTextToken,
            "user" => $user,
        ];
    }

    public function logout(Request $request)
    {
        $user = $request->user();
        $user->tokens()->delete();
    }
}
