<?php

namespace App\Http\Controllers;

use App\Exceptions\ApiException;
use App\Models\Like;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;

use function PHPUnit\Framework\isNull;

class ProductController extends Controller
{
    protected static function checkExpired(Product $product)
    {
        return time() >= $product->expiry_date;
    }

    protected static function calculatePrice(Product $product)
    {
        $remaining_days = ($product->expiry_date - time()) / (24 * 3600);
        
        if ($remaining_days <= 0)
            return 0;
        elseif ($remaining_days <= $product->second_period_days)
            return $product->unit_price * (1 - $product->second_period_sale/100);
        elseif ($remaining_days <= $product->first_period_days)
            return $product->unit_price * (1 - $product->first_period_sale/100);
        else
            return $product->unit_price * (1 - $product->initial_sale/100);
    }

    public function createProduct(Request $request, ImageController $imageController)
    {
        $validated = $request->validate([
            'image' => 'required|string',
            'details.name' => 'required|string',
            'details.description' => 'required|string',
            'details.available_quantity' => 'required|numeric|integer|min:0',
            'details.category' => 'required|string',
            'details.expiry_date' => 'required|numeric|integer|min:0',
            'details.unit_price' => 'required|numeric|min:0',
            'details.contact_phone' => 'required|string|regex:/(^\+963\d{9}$)/',
            'details.initial_sale' => 'required|numeric|integer|min:0|max:100',
            'details.first_period_days' => 'required|numeric|integer|min:0',
            'details.first_period_sale' => 'required|numeric|integer|min:0|max:100',
            'details.second_period_days' => 'required|numeric|integer|min:0',
            'details.second_period_sale' => 'required|numeric|integer|min:0|max:100',
        ]);

        $validated['details']['image_id'] = $imageController->insertImage($validated['image']);
        $validated['details']['created_by_id'] = $request->user()->id;

        return Product::create($validated['details']);
    }

    public function updateProduct(Request $request, int $product_id, ImageController $imageController)
    {
        $validated = $request->validate([
            'image' => 'string',
            'details.name' => 'required|string',
            'details.description' => 'required|string',
            'details.available_quantity' => 'required|numeric|integer|min:0',
            'details.category' => 'required|string',
            'details.unit_price' => 'required|numeric|min:0',
            'details.contact_phone' => 'required|string|regex:/(^\+963\d{9}$)/',
            'details.initial_sale' => 'required|numeric|integer|min:0|max:100',
            'details.first_period_days' => 'required|numeric|integer|min:0',
            'details.first_period_sale' => 'required|numeric|integer|min:0|max:100',
            'details.second_period_days' => 'required|numeric|integer|min:0',
            'details.second_period_sale' => 'required|numeric|integer|min:0|max:100',
        ]);

        $product = Product::findOrFail($product_id);

        if ($product->created_by_id != $request->user()->id)
            throw new ApiException(403, 5, 'Not allowed to modify other users products.');

        if (isset($validated['image']))
            $product->image_id = $imageController->replaceImage($product->image_id, $validated['image']);
        $product->update($validated['details']);

        return $product;
    }

    public function deleteProduct(int $product_id, Request $request, ImageController $imageController)
    {
        $product = Product::findOrFail($product_id);
        if ($product->created_by_id != $request->user()->id)
            throw new ApiException(403, 5, 'Not allowed to modify other users products.');

        $imageController->deleteImage($product->image_id);
        $product->delete();
    }

    public function likeProduct(int $product_id, Request $request)
    {
        $user_id = $request->user()->id;
        $product = Product::findOrFail($product_id);

        if (!is_null(Like::firstWhere('user_id', $user_id))) return; // Already liked, don't fail for the developers convinence.

        Like::create([
            'product_id' => $product_id,
            'user_id' => $user_id,
        ]);

        $product->likes_count++;
        $product->save();
    }

    public function unlikeProduct(int $product_id, Request $request)
    {
        $product = Product::findOrFail($product_id);
        $like = Like::firstWhere('user_id', $request->user()->id);

        if (is_null($like)) return; // Already not liked, don't fail for the developers convinence.
        $like->delete();

        $product->likes_count--;
        $product->save();
    }

    public function getProductDetails(int $product_id, Request $request)
    {
        $user_id = $request->user()->id;
        $product = Product::findOrFail($product_id);

        $product->views_count++;
        $product->save();

        $product->liked = !is_null(Like::firstWhere('user_id', $user_id));
        $product->current_price = static::calculatePrice($product);
        
        $created_by = User::findOrFail($product->created_by_id);
        $product->created_by_name = $created_by->name;

        if ($product->created_by_id !== $user_id) {
            unset($product->initial_sale);
            unset($product->first_period_days);
            unset($product->first_period_sale);
            unset($product->second_period_days);
            unset($product->second_period_sale);
        }

        return $product;
    }

    public function getProducts(Request $request)
    {
        $products = Product::all();
        $response_products = [];

        $filter_name = $request->query('name');
        $filter_category = $request->query('category');
        $filter_expires_after = $request->query('expires_after');
        $sort_by = $request->query('sort_by');
        $sort_dir = $request->query('sort_dir', 'descending') === 'descending' ? 1 : -1;

        $user_id = $request->user()->id;

        foreach ($products as $product) {
            if (static::checkExpired($product))
                $product->delete();
            else
            {
                if ($filter_name && !str_contains($product->name, $filter_name)) continue;
                if ($filter_category && !str_contains($product->category, $filter_category)) continue;
                if ($filter_expires_after && $product->expiry_date < intval($filter_expires_after)) continue;
                if (str_ends_with($request->path(), 'users/me/products') && $product->created_by_id !== $user_id) continue;

                array_push($response_products, [
                    'id' => $product->id,
                    'name' => $product->name,
                    'image_id' => $product->image_id,
                    'category' => $product->category,
                    'expiry_date' => $product->expiry_date,
                    'current_price' => static::calculatePrice($product),
                ]);
            }
        }

        if ($sort_by === 'name')
            usort($response_products, function ($a, $b) use ($sort_dir) {
                return strcmp($a['name'], $b['name']) * $sort_dir;
            });
        elseif ($sort_by === 'category')
            usort($response_products, function ($a, $b) use ($sort_dir) {
                return strcmp($a['category'], $b['category']) * $sort_dir;
            });
        
        return $response_products;
    }
}
