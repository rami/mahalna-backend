<?php

namespace App\Http\Controllers;

use App\Exceptions\ApiException;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function getDetails(Request $request)
    {
        return $request->user();
    }


    public function addPhoto(Request $request, ImageController $imageController)
    {
        if (!$request->hasHeader('Content-Type', 'image/jpeg'))
            throw new ApiException(400, 7, 'The request is missing image data.');
        
        $user = $request->user();
        $image_data = base64_encode($request->getContent());

        if (is_null($user->image_id)) $user->image_id = $imageController->insertImage($image_data);
        else $user->image_id = $imageController->replaceImage($user->image_id, $image_data);
        
        $user->save();
    }

    public function removePhoto(Request $request, ImageController $imageController)
    {
        $user = $request->user();

        $imageController->deleteImage($user->image_id);
        unset($user->image_id);

        $user->save();
    }
}
