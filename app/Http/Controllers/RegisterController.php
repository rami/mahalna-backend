<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class RegisterController extends Controller
{
 
    public function register(Request $request){
        $validator = validator($request -> all(),[
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if($validator->fails()){
            return $this->sendError("Invalid input",$validator->errors());
        }

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $input['remember_token'] = Str::random(10);
        try{
            $user = new User($input);
            $user->save();
        }catch(\Exception $e){
            return $this->sendError("User already exist",'406');
        }
        
        $success['user_id'] = $user['id'];
         return $this->send_response($success,'User register success');
    }

    public function logIn(Request $request){
        
        // $request->validate([
        //     'email' => 'required',
        //     'password' => 'required',
        // ]);
   
        $validator = validator($request -> all(),[
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError("bad request",$validator->errors());
        }

        $userEmail = $request['email'];
        $userPassword = $request['password'];

        $user = User::where('email','=',$userEmail)->first();

        if(is_null($user)){
            return $this->sendError("user not found",[]);
        }

        if(Hash::check($userPassword, $user->password)){
            $success['user_id'] =  Str::random(10);
            return $this->send_response($success,"login has done successfuly");
        }else{
            return $this->sendError("incorrect password",[]);
        }
    }

    public function logOut(){
        return $this->send_response("","logout has done successfuly");
    }

    public function getProfile(Request $request,$id){
        $intId = (int)$id;
        $users = DB::table('users')
        ->select('*')
        ->where('id',$intId)
        ->get();
        $numUsers = count($users);
        if($numUsers > 0){
            $user = $users->get(0);
            return $this->send_response($user,$id);
        }else{
            return $this->sendError('407','incorrect id');
        }
    
    }


    //     if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
    //         $input = $request->all();
    //         $user = new User($input);
            
    //         //$success['token'] = $user->createToken('Obada')->accessToken;
    //         //$success['name'] = $user->name;
    //         return $this->send_response($success,'User register success');
    //     }else{
    //         return $this->sendError("Please verify your account",['error'=> 'Unauthorised']);
    //     }
    // }

    public function send_response($result, $message)
    {
        $response = [
            'success' => true,
            'data' => $result,
            'message' => $message
        ];
        return response()->json($response,200);
    }

    public function sendError($error, $errorMessage=[], $code = 404){
        $response = [
            'success' => false,
            'data' => $error
        ];
        if(!empty($errorMessage)){
            $response['data'] = $errorMessage;
        }
        return response()->json($response,200);
    }

}
