<?php

namespace App\Http\Controllers;

use App\Exceptions\ApiException;
use App\Models\Comment;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function create(int $product_id, Request $request) {
        $validated = $request->validate(['content' => 'required|string|filled|max:300']);
        Product::findOrFail($product_id);

        Comment::create([
            'user_id' => $request->user()->id,
            'product_id' => $product_id,
            'content' => $validated['content'],
        ]);
    }

    public function delete(int $comment_id, Request $request) {
        $comment = Comment::findOrFail($comment_id);
        if ($comment->user_id !== $request->user()->id)
            throw new ApiException(403, 6, 'Not allowed to delete comments of other users.');
        $comment->delete();
    }

    public function getAll(int $product_id) {
        $comments = Product::findOrFail($product_id)->comments()->get();

        foreach ($comments as $comment) {
            $comment->user_name = User::findOrFail($comment->user_id)->name;
        }

        return $comments;
    }
}
