<?php

namespace App\Http\Controllers;

use App\Models\Image;

class ImageController extends Controller
{
    public function insertImage(string $content)
    {
        return Image::create(['data' => base64_decode($content)])->id;
    }

    public function getImage(string $image_id)
    {
        $image = Image::findOrFail($image_id);
        return response($image->data, 200, ['Content-Type' => 'image/jpeg']);
    }

    public function replaceImage(string $image_id, string $content)
    {
        Image::findOrFail($image_id)->delete();
        return Image::create(['data' => base64_decode($content)])->id;
    }

    public function deleteImage(string $image_id)
    {
        Image::findOrFail($image_id)->delete();
    }
}
