<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->string('name');
            $table->text('description');
            $table->string('category');
            $table->integer('available_quantity');
            $table->bigInteger('expiry_date');
            $table->unsignedBigInteger('image_id');
            $table->unsignedBigInteger('created_by_id');
            $table->string('contact_phone');
            $table->integer('views_count');
            $table->integer('likes_count');
            $table->integer('unit_price');
            $table->integer('initial_sale');
            $table->integer('first_period_days');
            $table->integer('first_period_sale');
            $table->integer('second_period_days');
            $table->integer('second_period_sale');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
