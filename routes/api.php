<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/auth/register', [AuthController::class, 'register']);
Route::post('/auth/login', [AuthController::class, 'login']);

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::post('/auth/logout', [AuthController::class, 'logout']);

    Route::get('/users/me', [UserController::class, 'getDetails']);
    Route::post('/users/me/image', [UserController::class, 'addPhoto']);
    Route::delete('/users/me/image', [UserController::class, 'removePhoto']);

    Route::get('/users/me/products', [ProductController::class, 'getProducts']); // hackish solution here.
    
    Route::get('/products', [ProductController::class, 'getProducts']);
    Route::post('/products', [ProductController::class, 'createProduct']);
    Route::get('/products/{product_id}', [ProductController::class, 'getProductDetails']);
    Route::put('/products/{product_id}', [ProductController::class, 'updateProduct']);
    Route::delete('/products/{product_id}', [ProductController::class, 'deleteProduct']);

    Route::post('/products/{product_id}/like', [ProductController::class, 'likeProduct']);
    Route::delete('/products/{product_id}/like', [ProductController::class, 'unlikeProduct']);

    Route::get('/products/{product_id}/comments', [CommentController::class, 'getAll']);
    Route::post('/products/{product_id}/comments', [CommentController::class, 'create']);
    Route::delete('/comments/{comment_id}', [CommentController::class, 'delete']);

    Route::get('/images/{image_id}', [ImageController::class, 'getImage']);
});
